package com.company;

import java.io.*;
import java.nio.Buffer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Formatter;
import java.util.Properties;
import java.util.StringJoiner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    public static void main(String[] args) {
        //doStringJoiner();
        //doSimpleThread();
        //doThreadPool();
        doWriteFile();

    }

    public static void doWriteFile() {
        try ( BufferedWriter w = Files.newBufferedWriter(Paths.get("/Users/bastien/Documents/test_file.txt")) ) {
            w.write("Hello world !");
            w.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void doStringJoiner() {
        StringJoiner sj = new StringJoiner(", ", "{", "}");

        sj.add("Bastien");
        sj.add("Benjamin");
        sj.add("Benoit");

        System.out.println(sj.toString());
    }

    public static void doStringBuilder() {
        StringBuilder sb = new StringBuilder();

        sb.append("test, ");
        sb.append("azeazeaze, ");
        sb.append("zaezaedsc");
        String result = sb.toString();

        System.out.println(result);
        // StringBuild is more efficient than adding string like string1 + string2 = string3
    }

    public static void doWriteFormat(int david, int dawson, int dillon, int gordon, double avgDiff) throws IOException {
        BufferedWriter writer = Files.newBufferedWriter(Paths.get("/Users/bastien/Documents/test_file.txt"));
        try (Formatter f = new Formatter(writer)) {
//            %[argument index][flags][width][precision] conversion
            f.format("My nephews are %04d, %-4d, %d, and %d years old", david, dawson, dillon, gordon);
            f.format("The average age between each is %.1f years", avgDiff);
        }
    }

    public static void doSimpleProperties() {
        Properties props = new Properties();

        props.setProperty("displayName", "Jim Wilson");
        props.setProperty("accountNumber", "123-456-789");

        try (Writer writer = Files.newBufferedWriter(Paths.get("/Users/bastien/Documents/test_properties_file.properties"))) {
            props.store(writer, "My Comment");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void doXMLProperties() {
        Properties props = new Properties();

        props.setProperty("displayName", "Jim Wilson");
        props.setProperty("accountNumber", "123-456-789");

        try (OutputStream writer = Files.newOutputStream(Paths.get("/Users/bastien/Documents/test_properties_file.properties"))) {
            props.storeToXML(writer, "My Comment");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void readSimpleProperties() {
        Properties props = new Properties();

        try (Reader reader = Files.newBufferedReader(Paths.get("/Users/bastien/Documents/test_properties_file.properties"))) {

            props.load(reader);

            System.out.println(props.getProperty("displayName"));
            System.out.println(props.getProperty("accountNumber"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void readXMLProperties() {
        Properties props = new Properties();

        try (InputStream reader = Files.newInputStream(Paths.get("/Users/bastien/Documents/test_properties_file.properties"))) {

            props.loadFromXML(reader);

            System.out.println(props.getProperty("displayName"));
            System.out.println(props.getProperty("accountNumber"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void doSimpleThread() {
        Thread thread = new Thread(new DoRunnableSync());
        thread.start();
    }

    public static void doThreadPool() {

        ExecutorService es = Executors.newFixedThreadPool(3);
        DoRunnableSync dr = new DoRunnableSync();

        for (int i = 0; i < 3; i++) {
            /*es.submit(new Runnable() {
                public void run() {
                    System.out.println("Displaying message using a ThreadPool of 3 threads");
                }
            });*/
            es.submit(dr);
        }
        es.shutdown();
    }
}
