package com.company;

public class DoRunnableSync implements Runnable {

    private int amount = 0;

    DoRunnableSync() {
    }

    public synchronized int getAmount() {
        return amount;
    }

    private synchronized void IncAmont() {
        amount += 10;
    }

    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println("Before : " + getAmount());
            IncAmont();
            System.out.println("After : " + getAmount());
        }
    }
}
